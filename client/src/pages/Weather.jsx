import React, { useState, useEffect } from 'react';
import axios from 'axios';
import WeatherComponent from './WeatherComponent';
import { MAIN_API_URL } from '../config/ApiUrls';

const Weather = () => {
    const [appState, setAppState] = useState();
    useEffect(() => {
      const apiUrl = MAIN_API_URL + "WeatherForecast";
      axios.get(apiUrl).then((resp) => {
        const dt = resp.data;
        setAppState(dt);
      });
    }, [setAppState]);
    return (
       <WeatherComponent data={appState} />
    )
  };

export default Weather;
