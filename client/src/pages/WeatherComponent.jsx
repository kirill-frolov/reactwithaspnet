import React from 'react';

const WeatherComponent = ({data})=>{

    if (!data || data.length === 0) return <p>no data.</p>
    return (
<div>
    <table>
    <thead>
      <tr>
        <th>Date</th>
        <th>Temp. (C)</th>
        <th>Temp. (F)</th>
        <th>Summary</th>
      </tr>
    </thead>
    <tbody>
      {data.map(forecast =>
        <tr key={forecast.date}>
          <td>{forecast.date}</td>
          <td>{forecast.temperatureC}</td>
          <td>{forecast.temperatureF}</td>
          <td>{forecast.summary}</td>
        </tr>
      )}
    </tbody>
  </table>
  </div>
  )
}
export default WeatherComponent;