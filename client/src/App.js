import Home from './pages/Home';
import Weather from './pages/Weather'
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from 'react-router-dom'


function App() {
  return (

    <Router>
    <Routes>
      <Route exact path="/" element={<Home />} />
      <Route exact path="/weather" element={<Weather />} />
      <Route exact path="/*" element={<Home />} />
    </Routes>
  </Router>
   
  );
}

export default App;
